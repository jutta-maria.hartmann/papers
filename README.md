# Papers
This project provides access to papers and manuscripts by Jutta M. Hartmann (Bielefeld University). If not noted otherwise, these papers are copyright by Jutta M. Hartmann (Bielefeld).
For questions, please send an email to jutta-maria -- DOT -- hartmann -- AT -- uni-bielefeld -- DOT --de.
